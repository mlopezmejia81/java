Este proyecto es un CRUD (Create, Read, Update, Delete) de alumnos y cursos de una escuela.
Contiene relaciones SQL "many to many".
Esta configurado para operar en el puerto 8080.
Para iniciar el proyecto tienes que dirigirte a la siguiente direccion: CRUD_SPRING_MVC/src/main/java/com.proyecto.main/MainApplication.java
y en este archivo inicializas el proyecto.
En el navegador solo debes ingresar la siguiente direccion: http://localhost:8080/curso/lista