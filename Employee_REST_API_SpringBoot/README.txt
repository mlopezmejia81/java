Este proyecto es un servicio REST para obtener los datos de empleados de una empresa, poder crear nuevos registros, modificarlos y eliminarlos.
Los metodos implementados en este proyecto son:
* POST
* GET
* PUT
* DELETE

Para correr este proyecto deberas ingresar a la siguiente direccion: Employee_REST_API_SpringBoot\src\main\java\com\restapi\springboot\cruddemo\rest/EmployeeRestController

Para accdeder a traves de POSTMAN a este proyecto deberas ingresar alguna de las siguientes direcciones y seleccionar el tipo de request:
* http://localhost:8080/api/employees
* http://localhost:8080/api/employees/{id empleado}
