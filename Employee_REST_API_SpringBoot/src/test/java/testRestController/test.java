package testRestController;

import static org.junit.Assert.*;

import static org.mockito.Mockito.when;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.luv2code.springboot.cruddemo.dao.EmployeeDAOHibernateImpl;
import com.luv2code.springboot.cruddemo.entity.Employee;
import com.luv2code.springboot.cruddemo.service.EmployeeServiceImpl;
@WebAppConfiguration
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = EmployeeServiceImpl.class)
public class test {

	@Autowired 
	private EmployeeServiceImpl servicemine;
	
	@MockBean
	private EmployeeDAOHibernateImpl repo;
	
	
	@Test(timeout=200)
	public void getsemployees() throws Exception{
		when(repo.findAll()).thenReturn(Stream
				.of(new Employee("Jose","jose", "jose"), new Employee("pepe","pepe","pepe")).collect(Collectors.toList()));
		assertEquals(2,servicemine.findAll().size());
	}
	
	

}
