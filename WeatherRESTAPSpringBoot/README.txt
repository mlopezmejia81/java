Este es un servicio REST para obtener la temperatura en las ciudades del mundo usando diferentes metodos que se enlistan a continuacion:

* http://localhost:8081/bycity/{country}/{city}
* http://localhost:8081/bycity/{cityID}
* http://localhost:8081/bycity/bylat&lon/{lat}/{lon}
* http://localhost:8081/bycity/byzipCode/{zip}/{countryCode}

El proyecto esta configurado para trabajar en el puerto 8081.