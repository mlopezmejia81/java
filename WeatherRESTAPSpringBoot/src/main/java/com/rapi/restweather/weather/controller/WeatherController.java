package com.rapi.restweather.weather.controller;


import java.util.List;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;


@Controller 
public class WeatherController {
	
	@RequestMapping(method=RequestMethod.GET, value = "/bycity/{country}/{city}")
	//@ResponseBody 
	public static Object  getWeatherByCity(@PathVariable String city, @PathVariable String country) {
		
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		
		ResponseEntity<Object> response = restTemplate.
				getForEntity("https://api.openweathermap.org/data/2.5/weather?q=" + city + "," + country +
						"&appid=652b8bfae4948c53dfc0052cc9c31d4a",
						 
						Object.class);
		
		return response;
		
	}
	@RequestMapping(method=RequestMethod.GET, value = "/bycity/{cityID}")
	//@ResponseBody 
	public static Object  getWeatherByCityID(@PathVariable int cityID) {
		
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		
		ResponseEntity<Object> response = restTemplate.
				getForEntity("https://api.openweathermap.org/data/2.5/weather?id=" + cityID +
						"&appid=652b8bfae4948c53dfc0052cc9c31d4a",
						 
						Object.class);
		
		return response;
		
	}
	
	@RequestMapping(method=RequestMethod.GET, value = "/bycity/bylat&lon/{lat}/{lon}")
	//@ResponseBody 
	public static Object  getWeatherByGeographicCoordinates(@PathVariable Integer lon, @PathVariable Integer lat) {
		
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		
		ResponseEntity<Object> response = restTemplate.
				getForEntity("https://api.openweathermap.org/data/2.5/weather?lat=" + lat + "&lon=" + lon +
						"&appid=652b8bfae4948c53dfc0052cc9c31d4a",
						 
						Object.class);
		
		return response;
		
	}
	
	@RequestMapping(method=RequestMethod.GET, value = "/bycity/byzipCode/{zip}/{countryCode}")
	//@ResponseBody 
	public static Object  getWeatherByZipCode(@PathVariable Integer zip, @PathVariable String countryCode) {
		
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		
		ResponseEntity<Object> response = restTemplate.
				getForEntity("https://api.openweathermap.org/data/2.5/weather?zip=" + zip + "," + countryCode + 
						"&appid=652b8bfae4948c53dfc0052cc9c31d4a",
						 
						Object.class);
		
		return response;
		
	}


	
}
